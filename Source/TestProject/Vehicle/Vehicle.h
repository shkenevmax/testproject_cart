// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/ConstraintInstance.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "TestProject/Vehicle/VehicleInterface.h"
#include "Vehicle.generated.h"

UCLASS()
class TESTPROJECT_API AVehicleCPP : public AActor, public IVehicleInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVehicleCPP();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		USkeletalMeshComponent* VehicleSkeleton;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		UBoxComponent* BoxOfCollision_1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		UBoxComponent* BoxOfCollision_2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		UPhysicsConstraintComponent* PhysicsConstraintComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

UPROPERTY()
	bool bIsFront = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		virtual void ConnectComponents(UPrimitiveComponent* OtherComp) override;
	UFUNCTION()
		virtual void SeparateComponents() override;
	UFUNCTION()
		bool GetFront();

	UFUNCTION()
		void OnBoxOfCollision_1BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBoxOfCollision_2BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);
};
