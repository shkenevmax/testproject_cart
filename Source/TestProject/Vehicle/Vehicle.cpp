// Fill out your copyright notice in the Description page of Project Settings.


#include "TestProject/Vehicle/Vehicle.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "PhysicsEngine/ConstraintInstance.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AVehicleCPP::AVehicleCPP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VehicleSkeleton = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VehicleSkeleton"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> CarMesh(TEXT("/Game/Vehicle/Sedan/Sedan_SkelMesh.Sedan_SkelMesh"));
	VehicleSkeleton->SetSkeletalMesh(CarMesh.Object);
	VehicleSkeleton->SetupAttachment(RootComponent);
	VehicleSkeleton->bCastDynamicShadow = false;
	VehicleSkeleton->CastShadow = false;
	VehicleSkeleton->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));
	static ConstructorHelpers::FClassFinder<UObject> AnimBPClass(TEXT("/Game/Vehicle/Sedan/Sedan_AnimBP"));
	VehicleSkeleton->SetAnimInstanceClass(AnimBPClass.Class);
	VehicleSkeleton->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	VehicleSkeleton->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	VehicleSkeleton->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	VehicleSkeleton->SetSimulatePhysics(false);


	BoxOfCollision_1 = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxOfCollision_1"));
	BoxOfCollision_1->SetupAttachment(VehicleSkeleton);
	BoxOfCollision_1->SetRelativeLocation(FVector(240.0f, 0.0f, 60.0f));
	BoxOfCollision_1->SetRelativeScale3D(FVector(1.0f, 2.75f, 1.0f));
	BoxOfCollision_1->OnComponentBeginOverlap.AddDynamic(this, &AVehicleCPP::OnBoxOfCollision_1BeginOverlap);

	BoxOfCollision_2 = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxOfCollision_2"));
	BoxOfCollision_2->SetupAttachment(VehicleSkeleton);
	BoxOfCollision_2->SetRelativeLocation(FVector(-240.0f, 0.0f, 60.0f));
	BoxOfCollision_2->SetRelativeScale3D(FVector(1.0f, 2.75f, 1.0f));
	BoxOfCollision_2->OnComponentBeginOverlap.AddDynamic(this, &AVehicleCPP::OnBoxOfCollision_2BeginOverlap);

	PhysicsConstraintComp = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraintComp"));
	PhysicsConstraintComp->SetRelativeLocation(FVector(310.0f, 0.0f, 110.0f));
	PhysicsConstraintComp->SetLinearXLimit(ELinearConstraintMotion::LCM_Limited, 10.0f);
	PhysicsConstraintComp->SetLinearYLimit(ELinearConstraintMotion::LCM_Limited, 10.0f);
	PhysicsConstraintComp->SetLinearZLimit(ELinearConstraintMotion::LCM_Limited, 10.0f);
}

// Called when the game starts or when spawned
void AVehicleCPP::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVehicleCPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVehicleCPP::ConnectComponents(UPrimitiveComponent* OtherComp)
{
	VehicleSkeleton->SetSimulatePhysics(true);
	PhysicsConstraintComp->SetConstrainedComponents(VehicleSkeleton, "None", OtherComp, "None");
}

void AVehicleCPP::SeparateComponents()
{
	 VehicleSkeleton->SetSimulatePhysics(false);
	 PhysicsConstraintComp->SetConstrainedComponents(nullptr, "None", nullptr, "None");
}

bool AVehicleCPP::GetFront()
{
	return bIsFront;
}

void AVehicleCPP::OnBoxOfCollision_1BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult)
{
	bIsFront = true;
}

void AVehicleCPP::OnBoxOfCollision_2BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult)
{
	bIsFront = false;
}
